import React, { Component } from 'react';
import * as tudo from 'react-native';
import axios from 'axios';

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            id: props.user.userId,
            user: props.user.userLogged,
            oldPassword: '',
            newPassword: '',
            newPasswordC: ''
        }
    }

    componentDidMount(){
        console.log(this.state.id);
    }

    alter = (oldPassword, newPassword, newPasswordC) => {
        if (newPassword != newPasswordC) {
            tudo.Alert.alert(
                'Atualizar perfil',
                'As passwords inseridas não coincidem',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        }  else {
            return axios.post("http://192.168.1.9:3000/alterUser", {
                id: this.state.id,
                pswd: oldPassword,
                passwordN: newPassword
            })
                .then(function (response) {
                    if (response.data.status == true) {
                        tudo.Alert.alert(
                            'Atualizar perfil',
                            'Password atualizada com sucesso',
                            [
                                { text: 'Ok', onPress: () => this.goToMapPage() },
                            ]
                        )
                    }else{
                        tudo.Alert.alert(
                            'Atualizar perfil',
                            'Password incorreta',
                            [
                                { text: 'Ok', style: 'cancel' },
                            ]
                        )
                    }
                }.bind(this))
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    goToMapPage() {
        this.state.navigation.replace('MapDrawer', { 
            screen: 'Map',
        });
    }


    render() {
        return (
            <tudo.View style={styles.full}>
                <tudo.View style={styles.imagePart}>
                    <tudo.Image
                        style={styles.imageP}
                        source={require('../images/logo.png')}
                    />
                    <tudo.Text style={styles.text}>Editar</tudo.Text>
                </tudo.View>
                

                <tudo.View style={styles.newPassword}>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Password antiga'
                        onChangeText={(oldPassword) => this.setState({ oldPassword })}></tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Nova password'
                        onChangeText={(newPassword) => this.setState({ newPassword })}></tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Confirmar nova password'
                        onChangeText={(newPasswordC) => this.setState({ newPasswordC })}></tudo.TextInput>
                </tudo.View>

                <tudo.View style={styles.buttonview}>
                    <tudo.TouchableOpacity
                        style={styles.Logout}
                        onPress={() => this.alter(this.state.oldPassword, this.state.newPassword, this.state.newPasswordC)}>
                        <tudo.Text style={styles.imageButton}>Confirmar</tudo.Text>
                    </tudo.TouchableOpacity>
                </tudo.View>
            </tudo.View>
        )
    }
}


const styles = tudo.StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },text: {
        color: 'black',
        fontSize: 25,
    },
    imagePart: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    newPassword: {
        margin: 40,
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Logout: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5
    },
    textinput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
    },
    imageP: {
        marginTop: 40,
        marginBottom: 30,
        flex: 1,
        width: 112,
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
    },
})

export default Profile;