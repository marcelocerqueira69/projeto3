import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, Alert, Button, Dimensions } from 'react-native';
import axios from 'axios';

const { width: WIDTH } = Dimensions.get('window');

class Fila extends Component {
    constructor(props) {
        super(props);

        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);

        this.state = {
            navigation: props.navigation,
            idFila: props.route.params.idFila,
            numFila: props.route.params.numFila,
            user: props.route.params.user,
            full: props.route.params.full,
            count: props.route.params.count,
            userLogged: props.route.params.userLogged,
            usersConf: props.route.params.usersConf
        }
    }

    forceUpdateHandler(string) {
        this.setState({ user: this.state.userLogged })
        if (string == 'false') {
            this.state.full ? this.setState({ full: false }) : this.setState({ full: true });
            this.state.count = 0;
        } else {
            this.state.count++;
        }
        axios({
            method: 'POST',
            url: 'http://192.168.1.9:3000/editline',
            data: {
                id: this.state.idFila,
                user: this.state.user,
                full: this.state.full,
                count: this.state.count
            }
        })
            .then((res) => {
                this.forceUpdate();
            }, (error) => {
                console.log('ups' + error);
            })
        
            axios({
                method: 'POST',
                url: 'http://192.168.1.9:3000/getspecificline',
                data: {
                    id: this.state.idFila
                }
              })
                .then(function (res) {

                console.log(res.data);
          
                  this.setState({
                    usersConf: res.data.usersConf
                  })

                  console.log("after GET");
                  console.log(this.state.usersConf);
          
                }.bind(this))
                .catch((error) => {
                  console.log('erro:' + error);
                });
    }


    render() {
        return (
            <View style={styles.full}>
                <View style={styles.textView}>
                    {this.state.full ?
                        <Text style={styles.textFull}>Esta fila está ocupada...</Text> :
                        <Text style={styles.textLivre}>Esta fila está livre!</Text>}

                    <Text style={styles.textUser}>Ultima atualização feita por {this.state.user}</Text>
                    <Text style={styles.textUser}>Número de confirmações: {this.state.count}</Text>
                </View>
                <View style={styles.buttonview}>
                    <TouchableOpacity
                        style={styles.Logout}
                        onPress={() => {
                            let exist = false;
                            //console.log(this.state.usersConf);
                            for (var i = 0; i < this.state.usersConf.length; i++) {
                                //console.log(this.state.usersConfirmed[i]);
                                if (this.state.usersConf[i] === this.state.userLogged) {
                                    exist = true;
                                }
                            }

                            if (exist) {
                                Alert.alert(
                                    'Falha',
                                    'Não pode confirmar mais do que uma vez!',
                                    [
                                        { text: 'Ok' },
                                    ]
                                )
                            } else {
                                Alert.alert(
                                    'Confirmação',
                                    'Obrigado pela sua confirmação :)',
                                    [
                                        { text: 'Ok', onPress: () => this.forceUpdateHandler('true') },
                                    ]
                                )
                            }

                        }}>
                        <Text style={styles.imageButton}>Confirmar</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.Logout}
                        onPress={() => Alert.alert(
                            'Correção',
                            'De certeza que a informação está errada?',
                            [
                                { text: 'Não', onPress: () => console.log('k bich') },
                                { text: 'Sim', onPress: () => this.forceUpdateHandler('false') },
                            ]
                        )}>
                        <Text style={styles.imageButton}>Corrigir</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    textLivre: {
        alignContent: 'center',
        color: 'green',
        fontSize: 36
    },
    textFull: {
        color: 'red',
        fontSize: 36
    },
    textUser: {
        color: 'gray',
        fontSize: 18,
        marginTop: '1%'
    },
    textView: {
        flex: 0.5,
        marginTop: '20%',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    buttonView: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    button: {
        width: WIDTH - 200,
        backgroundColor: '#69dbd4',
        marginTop: '30%'
    },
    Logout: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5,
        margin: 10
    },
})

export default Fila;