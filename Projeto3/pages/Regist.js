import React, { Component } from 'react';
import * as tudo from 'react-native';
import axios from 'axios';

class Regist extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigation: props.navigation,
            name: '',
            email: '',
            password: '',
        }
    }

    regist = (name, email, password) => {
        if (name == '' || email == '' || password == '') {
            tudo.Alert.alert(
                'Login',
                'Tem de inserir todos os campos',
                [
                    { text: 'Ok', style: 'cancel' },
                ]
            )
        } else {
            return axios.post("http://192.168.1.67:3000/register", {
                name: name,
                email: email,
                pswd: password
            })
                .then(function (response) {
                    if (response.status == 200) {
                        tudo.Alert.alert(
                            'Registo',
                            'Registo efetuado com sucesso',
                            [
                                { text: 'Ok', onPress: () => this.goToLogin() },
                            ]
                        )
                    } else {
                        tudo.Alert.alert(
                            'Login',
                            'Login falhou',
                            [
                                { text: 'Ok', style: 'cancel' },
                            ]
                        )
                    }
                }.bind(this))
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    goToLogin(){
        this.state.navigation.replace('Login');
    }

    render() {
        return (
            <tudo.View style={styles.full}>
                <tudo.View style={styles.imagePart}>
                    <tudo.Image
                        style={styles.imageP}
                        source={require('../images/add-user.png')}
                    />
                    <tudo.Text styles={styles.text}>Registo</tudo.Text>
                </tudo.View>
                <tudo.View style={styles.insertAndButton}>
                    <tudo.TextInput
                        style={styles.textinput}
                        placeholder='Nome'
                        onChangeText={(name) => this.setState({ name })}>
                    </tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput}
                        placeholder='Email'
                        onChangeText={(email) => this.setState({ email })}>
                    </tudo.TextInput>
                    <tudo.TextInput
                        style={styles.textinput} secureTextEntry={true}
                        placeholder='Password'
                        onChangeText={(password) => this.setState({ password })}>
                    </tudo.TextInput>
                    <tudo.TouchableOpacity
                            style={styles.button}
                            onPress={() => this.regist(this.state.name, this.state.email, this.state.password)}>
                            <tudo.Text style={styles.imageButton}>Registar</tudo.Text>
                        </tudo.TouchableOpacity>
                </tudo.View>
            </tudo.View>

        )
    }
}

const styles = tudo.StyleSheet.create({
    full: {
        flex: 1,
        flexDirection: 'column',
    },
    imagePart: {
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    insertAndButton: {
        margin: 40,
        flex: 2.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageP: {
        marginTop: 40,
        flex: 1,
        width: 170,
    },
    text: {
        color: 'black',
        fontSize: 40
    },
    textinput: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
    },
    buttonview: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    button: {
        margin:20,
        alignItems: 'center',
        justifyContent: 'center',
        height: 35,
        width: 130,
        backgroundColor: '#69dbd4',
        borderRadius: 5,
    },
})

export default Regist;