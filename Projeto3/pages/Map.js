import React, { Component } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout, Polygon } from 'react-native-maps';
import { StyleSheet, View, Text, TouchableOpacity, Image, Alert } from 'react-native';
import axios from 'axios';
import Geolocation from '@react-native-community/geolocation';
import {request, PERMISSIONS} from 'react-native-permissions';


class Map extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navigation: props.navigation,
      polyList: [],
      userLogged: props.user.userLogged
    }

  }

  watchID /*: ?number*/ = null;
  componentDidMount() {
    this.requestLocalPermission();
    this.fetchData();
    this.willFocus = this.state.navigation.addListener(
      'focus', () =>{
        console.log("MUAHAHAHAHAHH EU ENTREI");
        this.fetchData();
      }
    )   

    console.log(this.state.userLogged)
  }

  componentWillUnmount(){
    this.willFocus();
    Geolocation.clearWatch(this.watchID);
  }

  fetchData() {
    axios({
      method: 'GET',
      url: 'http://192.168.1.9:3000/getlines'
    })
      .then(function (res) {

        this.setState({
          polyList: res.data
        })

      }.bind(this))
      .catch((error) => {
        console.log('erro:' + error);
      });
  }

  requestLocalPermission = async() =>{
    var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    console.log('permission: ' + response);

    if(response === 'granted'){
      this.currentPosition();
    }
  }

  currentPosition = () =>{
    Geolocation.getCurrentPosition(
      position =>{
        console.log(JSON.stringify(position));

        let initialPosition = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.003,
          longitudeDelta: 0.003
        }

        this.setState({initialPosition});
      },
      error => Alert.alert(error.message),
      {enableHighAccuracy: true, timeout: 10000}
    )

    this.watchID = Geolocation.watchPosition(
      position =>{
        this.setState(position);
        const lastPosition = JSON.stringify(position);
        this.setState({lastPosition});
      },
      error => Alert.alert(error.message),
      {enableHighAccuracy: true, timeout: 10000, distanceFilter: 1}
    )
  }


  render() {
    return (
      <MapView
        provider={PROVIDER_GOOGLE}
        ref={map => this._map = map}
        showsUserLocation={true}
        mapType='satellite'
        style={styles.map}
        initialRegion={this.state.initialPosition}
      >

        {this.state.polyList.map(poligono => (<Polygon
            key={poligono.num}
            coordinates={poligono.coordinates}
            fillColor={poligono.full === true ? 'rgba(204, 0, 0, 0.5)' : 'rgba(0, 204, 0, 0.5)'}
            strokeColor={'rgba(150, 150, 150, 0.8)'}
            tappable={true}
            onPress={() => this.props.navigation.navigate('Fila', {
              idFila: poligono._id, 
              numFila: poligono.num, 
              user: poligono.user,
              full: poligono.full,
              count: poligono.count,
              userLogged: this.state.userLogged,
              usersConf: poligono.usersConf
          })}
          >
        </Polygon>))}

      </MapView>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 40
  },
  map: {
    height: '100%'
  },
  Logout: {
    alignItems: 'flex-end',
    marginRight: 40,
    bottom: "-1%"
  },
  imageButton: {
    width: 20,
    height: 20,
  },
})

export default Map;