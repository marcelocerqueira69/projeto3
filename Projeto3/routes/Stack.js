import * as React from 'react';


import { NavigationContainer, DrawerActions, useRoute } from '@react-navigation/native';
import { TouchableOpacity, Image, StyleSheet, View, Avatar, Text, Dimensions } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'; //queria por icons, couldnt do for now, sad

import Map from '../pages/Map';
import Login from '../pages/Login';
import Regist from '../pages/Regist';
import Fila from '../pages/Fila';
import Profile from '../pages/Profile';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const { height: HEIGHT } = Dimensions.get('window');

function CustomDrawerContent(props) {
    return (
        <DrawerContentScrollView {...props}>
            <View style={{height: 200}}>
                <Image style={{
                    width: 100,
                    height: 100,
                    marginTop: '10%',
                    alignSelf: 'center'
                }} rounded source={require('../images/logo.png')} />
                <Text style={{ 
                    color: '#69dbd4', 
                    marginTop: '5%', 
                    fontFamily: 'sans-serif-condensed', 
                    alignSelf: 'center', 
                    fontSize: 20 }}>Hello {props.user.userLogged}</Text>
            </View>
            <DrawerItemList {...props} />
            <DrawerItem
                style={{marginTop: HEIGHT - 450}}
                label="Logout"
                onPress={() => props.navigation.replace('Login')}
            />
        </DrawerContentScrollView>
    );
}



function Drawerr({ user }) {
    return (
        <Drawer.Navigator initialRouteName='Map' drawerContent={props => <CustomDrawerContent {...props}  user={user}/>}>
            <Drawer.Screen name="Map" options={{
                        title: 'Map'
                    }} >
                {props => <Map {...props} user={user}/>}
            </Drawer.Screen>
            <Drawer.Screen name="Edit Profile"  options={{
                        title: 'Edit Profile'
                    }} >
                {props => <Profile {...props} user={user} />}
            </Drawer.Screen>
        </Drawer.Navigator>
    )
}

function MainStack({ navigation }) {
    const [user, setUser] = React.useState({});
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='Login'>
                <Stack.Screen name='Login'
                    options={{
                        headerStyle: { backgroundColor: '#69dbd4' },
                    }} >
                    {props => <Login {...props} setUser={setUser} />}
                </Stack.Screen>
                <Stack.Screen name='Regist'
                    component={Regist}
                    options={{
                        headerStyle: { backgroundColor: '#69dbd4' },
                    }} />
                <Stack.Screen name='Fila'
                    component={Fila}
                    options={{
                        headerStyle: { backgroundColor: '#69dbd4' },
                    }} />
                <Stack.Screen name='MapDrawer'
                    options={({ navigation }) => ({
                        headerStyle: { backgroundColor: '#69dbd4' },
                        headerLeft: () => (
                            <TouchableOpacity
                                style={styles.Touch}
                                onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
                                <Image style={styles.imageButton} source={require('../images/open-menu.png')} />
                            </TouchableOpacity>
                        ),
                    })} >
                    {props => <Drawerr {...props} user={user} />}
                </Stack.Screen>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default MainStack;

const styles = StyleSheet.create({
    Touch: {
        marginLeft: 20
    },
    imageButton: {
        width: 20,
        height: 20,
    }
})